export class QuestionConfigurationDto {
  active: boolean;
  numberOfQuestions: number;
  points: number;
}
