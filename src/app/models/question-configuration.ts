export class QuestionConfiguration {
  id: number;
  numberOfQuestions: number;
  points: number;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
