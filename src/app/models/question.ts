import { User } from './user';

export class Question {
  id: number;
  content: string;
  answerA: string;
  answerB: string;
  answerC: string;
  answerD: string;
  correct: string;
  points: number;
  categoryName: string;
  author: User;
  createdAt: string;
  updatedAt: string;
}
