import {User} from '../user';
import {FormControl} from '@angular/forms';
import {Category} from '../category';

export class CategoryForm {
  id: string;
  name: FormControl;
  active: string;
  author: string;

  constructor(category?: Category) {
    category ? this.id = category.id.toString() : this.id = '';
    category ? this.active = String(category.active) : this.active = '';
    category ? this.author = `${category.author.firstName} ${category.author.lastName}` : this.author = '';
    category ? this.name = new FormControl(category.name) : this.name = new FormControl('');
  }
}
