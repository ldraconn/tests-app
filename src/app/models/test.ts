import { Answer } from './answer';
import { User } from './user';
export class Test {
  id: number;
  active: boolean;
  createdAt: string;
  expirationAt: string;
  now: string;
  totalPoints: number;
  status: string;
  maxPoints: number;
  answers: Answer[];
  totalWindowChanges: number;
  updatedAt: string;
  user: User;
  verdict: boolean;
  warning: boolean;
}
