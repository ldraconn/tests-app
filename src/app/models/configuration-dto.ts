import {QuestionConfigurationDto} from './question-configuration-dto';

export class ConfigurationDto {
  active: boolean;
  percent: number;
  questionsConfigurations: QuestionConfigurationDto[];
  time: string;
  totalWindowChangesAllow: number;
}
