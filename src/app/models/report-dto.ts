export class ReportDto {
  content: string
  questionId: number;

  constructor(content: string, questionId: number) {
    this.content = content;
    this.questionId = questionId;
  }
}
