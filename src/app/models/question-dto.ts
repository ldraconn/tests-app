export class QuestionDto {
  content: string;
  answerA: string;
  answerB: string;
  answerC: string;
  answerD: string;
  correct: string;
  points: number;
  categoryId: number;

  constructor(
    content?: string,
    answerA?: string,
    answerB?: string,
    answerC?: string,
    answerD?: string,
    correct?: string,
    points?: number,
    categoryId?: number
  ) {
    this.content ? this.content = content : '';
    this.answerA ? this.answerA = answerA : '';
    this.answerB ? this.answerB = answerB : '';
    this.answerC ? this.answerC = answerC : '';
    this.answerD ? this.answerD = answerD : '';
    this.correct ? this.correct = correct : '';
    this.points ? this.points = points : 0;
    this.categoryId ? this.categoryId = categoryId : 0;
  }
}
