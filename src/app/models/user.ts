import {AdminRole} from './enums/admin-role.enum';

export class User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  role: AdminRole;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
