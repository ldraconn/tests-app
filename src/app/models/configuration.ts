import {User} from './user';
import {QuestionConfiguration} from './question-configuration';

export class Configuration {
  id: number;
  time: string;
  questionConfigurations: QuestionConfiguration[];
  percent: number;
  totalWindowChangesAllow: number;
  author: User;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
