import { QuestionResponse } from './question-response';
export class Answer {
  id: number;
  answer: string;
  question: QuestionResponse;
  updatedAt: string;
  createdAt: string;
  active: boolean;
}
