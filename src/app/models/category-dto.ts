export class CategoryDto {
  active: boolean;
  name: string;

  constructor(name?: string, active?: boolean) {
    name ? this.name = name : this.name = '';
    active != null ? this.active = active : this.active = null;
  }
}
