import {AdminRole} from './enums/admin-role.enum';

export class UserDto {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  role: AdminRole;
}
