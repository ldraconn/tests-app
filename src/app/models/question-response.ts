export class QuestionResponse {
  id: number;
  category: string;
  points: number;
  content: string;
  answerA: string;
  answerB: string;
  answerC: string;
  answerD: string;
  correct: string;
}
