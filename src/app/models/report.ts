import {User} from './user';
import {Question} from './question';

export class Report {
  id: number;
  content: string;
  author: User;
  question: Question;
  moderator: User;
  positive: boolean;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
