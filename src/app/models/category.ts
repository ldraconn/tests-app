import {User} from './user';

export class Category {
  id: number;
  name: string;
  active: boolean;
  author: User;
}
