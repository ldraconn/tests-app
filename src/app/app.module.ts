import { QuestionModalContent } from './components/admin/questions-list/question-modal/question-modal.component';
import { AppInterceptor } from './utils/app-interceptor';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { TestComponent } from './components/test/test.component';
import { QuestionsListComponent } from './components/admin/questions-list/questions-list.component';
import { QuestionComponent } from './components/admin/questions-list/question/question.component';
import { CategoriesListComponent } from './components/admin/categories-list/categories-list.component';
import { CategoryComponent } from './components/admin/category/category.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuestionDeleteModal } from './components/admin/questions-list/question-delete-modal/question-delete-modal.component';
import { CanActivateAdminPage } from './utils/routes/canActivate/can-activate-admin-page';
import {CanActivateHasValidToken} from './utils/routes/canActivate/can-activate-has-valid-token';
import { MainNavbarComponent } from './components/main-navbar/main-navbar.component';
import { UsersListComponent } from './components/admin/users-list/users-list.component';
import { UserModalContent } from './components/admin/users-list/user-modal/user-modal.component';
import { UserDeleteModal } from './components/admin/users-list/user-delete-modal/user-delete-modal.component';
import { ArchiveListComponent } from './components/admin/archive-list/archive-list.component';
import { ArchiveModal } from './components/admin/archive-list/archive-modal/archive-modal.component';
import { ReportsListComponent } from './components/admin/reports-list/reports-list.component';
import { ReportComponent } from './components/admin/reports-list/report/report.component';
import { ReportModal } from './components/test/report-modal/report-modal.component';
import { ReportConsiderModal } from './components/admin/reports-list/report-consider-modal/report-consider-modal.component';
import { SortArrowsComponent } from './components/admin/sort-arrows/sort-arrows.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfigurationsListComponent } from './components/admin/configurations-list/configurations-list.component';
import { ConfigurationComponent } from './components/admin/configurations-list/configuration/configuration.component';
import { ConfigurationModal } from './components/admin/configurations-list/configuration-modal/configuration-modal.component';
import { ConfigurationDeleteModal } from './components/admin/configurations-list/configuration-delete-modal/configuration-delete-modal.component';
import { ConfigTimePipe } from './pipes/config-time.pipe';
import { PaginatorComponent } from './components/admin/paginator/paginator.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    TestComponent,
    QuestionsListComponent,
    QuestionComponent,
    QuestionModalContent,
    CategoriesListComponent,
    CategoryComponent,
    QuestionDeleteModal,
    MainNavbarComponent,
    UsersListComponent,
    UserModalContent,
    UserDeleteModal,
    ArchiveListComponent,
    ArchiveModal,
    ReportsListComponent,
    ReportComponent,
    ReportModal,
    ReportConsiderModal,
    SortArrowsComponent,
    ConfigurationsListComponent,
    ConfigurationComponent,
    ConfigurationModal,
    ConfigurationDeleteModal,
    ConfigTimePipe,
    PaginatorComponent
  ],
  entryComponents: [
    QuestionModalContent,
    QuestionDeleteModal,
    UserModalContent,
    UserDeleteModal,
    ArchiveModal,
    ReportModal,
    ReportConsiderModal,
    ConfigurationModal,
    ConfigurationDeleteModal
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatIconModule,
    FormsModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    CanActivateAdminPage,
    CanActivateHasValidToken,
    { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
