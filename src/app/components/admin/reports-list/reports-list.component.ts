import {Component, OnInit} from '@angular/core';
import {Question} from '../../../models/question';
import {QuestionsService} from '../../../services/questions.service';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {QuestionModalContent} from '../questions-list/question-modal/question-modal.component';
import {Report} from '../../../models/report';
import {ReportsService} from '../../../services/reports.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-reports-list',
  templateUrl: './reports-list.component.html',
  styleUrls: ['./reports-list.component.scss']
})
export class ReportsListComponent implements OnInit {

  isLoaded: boolean;
  reports: Report[];
  pages: number;

  currentReport: number;
  currentPage: number;

  constructor(public service: ReportsService,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
    this.reports = [];
    this.currentReport = -1;
    this.isLoaded = false;
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.service.getList(1, 10, 'id', 'asc').then(response => {
      this.reports = response.content;
      this.pages = response.totalPages;
      this.isLoaded = true;
    }).catch(() => this.isLoaded = true);
  }

  setNewPage(reportsList: Report[]) {
    this.currentReport = -1;
    this.reports = reportsList;

  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }

  refresh(result) {
    this.service.getList(this.currentPage, 10, 'id', 'asc').then(response => {
      this.reports = response.content;
      this.pages = response.totalPages;
      this.showAlert(result);
    });
  }

  showAlert(result) {
    switch (result) {
      case 'considered': {
        alert('Rozpatrzono zgłoszenie');
        break;
      }
    }
  }

  toggle(index: number) {
    if (this.currentReport === index) {
      this.currentReport = -1;
    } else {
      this.currentReport = index;
    }
  }

}
