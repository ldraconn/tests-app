import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../../../models/question';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Report} from '../../../../models/report';
import {ReportConsiderModal} from '../report-consider-modal/report-consider-modal.component';
import {ReportsService} from '../../../../services/reports.service';
import {QuestionModalContent} from '../../questions-list/question-modal/question-modal.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  @Input() report: Report;
  @Output() closed = new EventEmitter<any>();
  question: Question;

  constructor(private modalService: NgbModal,
              private reportsService: ReportsService) {
  }

  ngOnInit() {
  }

  openConsiderModal() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modalRef = this.modalService.open(ReportConsiderModal, ngbModalOptions);
    modalRef.componentInstance.reportId = this.report.id;
    modalRef.result.then(result => {
      if (result instanceof Object) {
        const questionRef = this.modalService.open(QuestionModalContent, ngbModalOptions);
        questionRef.componentInstance.question = result;
        questionRef.result.then(() => {
          this.closed.emit('considered');
        }).catch(() => {
          this.closed.emit('considered');
        });
      } else {
        this.closed.emit(result);
      }
    });
  }
}
