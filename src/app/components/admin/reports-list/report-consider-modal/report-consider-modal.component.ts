import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {QuestionsService} from '../../../../services/questions.service';
import {ReportsService} from '../../../../services/reports.service';
import {Report} from '../../../../models/report';
import {Question} from '../../../../models/question';

@Component({
  selector: 'app-report-consider-modal',
  templateUrl: './report-consider-modal.component.html',
  styleUrls: ['./report-consider-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ReportConsiderModal implements OnInit {

  reportId: number;

  constructor(public activeModal: NgbActiveModal,
              private reportsService: ReportsService,
              private questionsService: QuestionsService) {
  }

  ngOnInit() {
  }

  positiveConsider() {
    this.reportsService.moderateReport(this.reportId, true).then(rep => {
      this.questionsService.getOne(rep.question.id).then(question => {
        this.activeModal.close(question as Question);
      });
    });
  }

  negativeConsider() {
    this.reportsService.moderateReport(this.reportId, false).then(() => {
      this.activeModal.close('considered');
    });
  }

}
