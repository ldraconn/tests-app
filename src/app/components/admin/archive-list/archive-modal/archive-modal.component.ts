import {Component, HostBinding, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Test} from '../../../../models/test';
import {ArchiveService} from '../../../../services/archive.service';
import {ReportModal} from '../../../test/report-modal/report-modal.component';

@Component({
  selector: 'app-archive-modal',
  templateUrl: './archive-modal.component.html',
  styleUrls: ['./archive-modal.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.modal-content]': 'true'
  }
})
// tslint:disable-next-line:component-class-suffix
export class ArchiveModal implements OnInit {

  testId: number;
  test: Test;

  constructor(public activeModal: NgbActiveModal,
              private archiveService: ArchiveService,
              private modalService: NgbModal) {
    this.testId = -1;
    this.test = null;
  }

  ngOnInit() {
    this.archiveService.getOne(this.testId).then(response => {
      this.test = response;
    });
  }

  showReportForm(index: number) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modalRef = this.modalService.open(ReportModal, ngbModalOptions);
    modalRef.componentInstance.question = this.test.answers[index].question;
    modalRef.result.then(() => {
      alert('Zgłoszenie wysłane');
    });
  }

}
