import {Component, OnInit} from '@angular/core';
import {ArchiveService} from '../../../services/archive.service';
import {Test} from '../../../models/test';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ArchiveModal} from './archive-modal/archive-modal.component';
import {QuestionsService} from '../../../services/questions.service';
import {environment} from '../../../../environments/environment';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-archive-list',
  templateUrl: './archive-list.component.html',
  styleUrls: ['./archive-list.component.scss']
})
export class ArchiveListComponent implements OnInit {

  environment = environment;

  isLoaded: boolean;
  tests: Test[];
  pages: number;

  currentPage: number;

  constructor(public archiveService: ArchiveService,
              private questionsService: QuestionsService,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
    this.tests = [];
    this.pages = 0;
    this.isLoaded = false;
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.archiveService.getList(1, 10).then(response => {
      this.tests = response.content;
      this.pages = response.totalPages;
      this.isLoaded = true;
    });
  }

  setNewPage(testsList: Test[]) {
    this.tests = testsList;
  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }


  showTest(index: number) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'modal-wide'
    };
    const modelRef = this.modalService.open(ArchiveModal, ngbModalOptions);
    modelRef.componentInstance.testId = this.tests[index].id;
    modelRef.result.then(reason => {

    });
  }
}
