import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sort-arrows',
  templateUrl: './sort-arrows.component.html',
  styleUrls: ['./sort-arrows.component.scss']
})
export class SortArrowsComponent implements OnInit {

  @Input() name: string;
  @Input() order: string;

  @Input() currentOrder: string;
  @Output() currentChange = new EventEmitter();

  @Input() currentDir: string;

  constructor() {
  }

  ngOnInit() {
  }

  sort() {
    if (this.currentOrder !== this.order) {
      this.currentOrder = this.order;
      this.currentDir = 'asc';
      this.currentChange.emit({order: this.currentOrder, dir: this.currentDir});
      return;
    }
    this.currentOrder = this.order;
    if (this.currentDir === 'asc') {
      this.currentDir = 'desc';
    } else if (this.currentDir === 'desc') {
      this.currentDir = 'asc';
    }
    this.currentChange.emit({order: this.currentOrder, dir: this.currentDir});
  }

}
