import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationService} from '../../../../services/configuration.service';
import {ConfigurationDto} from '../../../../models/configuration-dto';
import {Configuration} from '../../../../models/configuration';

@Component({
  selector: 'app-configuration-delete-modal',
  templateUrl: './configuration-delete-modal.component.html',
  styleUrls: ['./configuration-delete-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ConfigurationDeleteModal implements OnInit {

  config: Configuration;

  constructor(public activeModal: NgbActiveModal,
              private service: ConfigurationService) {
  }

  ngOnInit() {
  }

  toggleConfig() {

    const body = new ConfigurationDto();

    body.active = !this.config.active;
    body.percent = this.config.percent;
    body.time = this.config.time;
    body.totalWindowChangesAllow = this.config.totalWindowChangesAllow;

    this.service.modify(body, this.config.id).then(() => {
      this.activeModal.close('toggled');
    });
  }

}
