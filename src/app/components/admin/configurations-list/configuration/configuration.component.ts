import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../../../models/question';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {QuestionModalContent} from '../../questions-list/question-modal/question-modal.component';
import {QuestionDeleteModal} from '../../questions-list/question-delete-modal/question-delete-modal.component';
import {Configuration} from '../../../../models/configuration';
import {ConfigurationModal} from '../configuration-modal/configuration-modal.component';
import {ConfigurationDeleteModal} from '../configuration-delete-modal/configuration-delete-modal.component';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  @Input() configuration: Configuration;
  @Output() closed = new EventEmitter<any>();

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  openEditConfigModal(config: Configuration) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modelRef = this.modalService.open(ConfigurationModal, ngbModalOptions);
    modelRef.componentInstance.config = config;
    modelRef.result.then(result => {
      this.closed.emit(result);
    });
  }

  openToggleConfigModal(config: Configuration) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modelRef = this.modalService.open(ConfigurationDeleteModal, ngbModalOptions);
    modelRef.componentInstance.config = config;
    modelRef.result.then(result => {
      this.closed.emit(result);
    });
  }


}
