import {Component, OnInit} from '@angular/core';
import {Question} from '../../../../models/question';
import {FormArray, FormBuilder, FormControl, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfigurationDto} from '../../../../models/configuration-dto';
import {ConfigurationService} from '../../../../services/configuration.service';
import {QuestionConfigurationDto} from '../../../../models/question-configuration-dto';
import * as moment from 'moment';
import {Configuration} from '../../../../models/configuration';

@Component({
  selector: 'app-configuration-modal',
  templateUrl: './configuration-modal.component.html',
  styleUrls: ['./configuration-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ConfigurationModal implements OnInit {

  config: Configuration;

  pointsArray = new FormBuilder().array([
    ['', Validators.required],
    ['', Validators.required],
    ['', Validators.required],
    ['', Validators.required],
    ['', Validators.required]
  ]);

  configForm = new FormBuilder().group({
    time: ['', Validators.required],
    percent: ['', Validators.required],
    windowChanges: ['', Validators.required],
    points: this.pointsArray
  });


  isEdited = {
    time: false,
    percent: false,
    windowChanges: false,
    points: [false, false, false, false, false]
  };

  pointsChecked = [true, true, true, true, true];

  constructor(public activeModal: NgbActiveModal,
              private configService: ConfigurationService) {
  }

  ngOnInit() {
    if (this.config) {

      this.config.questionConfigurations.forEach(questionConf => {
        this.pointsArray.controls[questionConf.points - 1].setValue(questionConf.numberOfQuestions);
      });

      const timeMoment = moment(this.config.time, 'HH:mm');
      const time = timeMoment.hours() * 60 + timeMoment.minutes();
      this.configForm = new FormBuilder().group({
        time: [time, Validators.required],
        percent: [this.config.percent, Validators.required],
        windowChanges: [this.config.totalWindowChangesAllow, Validators.required],
      });
    }
  }

  togglePoints(index: number) {
    this.pointsChecked[index] = !this.pointsChecked[index];
  }

  validate() {
    this.isEdited = {
      time: true,
      percent: true,
      windowChanges: true,
      points: [true, true, true, true, true]
    };

    const pointsValid = false;
    this.pointsArray.controls.forEach(control => pointsValid || control.getError('required'));

    return !(this.configForm.get('time').getError('required') ||
      this.configForm.get('percent').getError('required') ||
      this.configForm.get('windowChanges').getError('required') ||
      pointsValid);
  }

  addConfig() {
    if (!this.validate()) {
      return;
    }
    const config = new ConfigurationDto();
    config.percent = this.configForm.get('percent').value;
    config.totalWindowChangesAllow = this.configForm.get('windowChanges').value;

    let time = this.configForm.get('time').value;
    time = `${(time - time % 60) / 60}:${time % 60}`;
    config.time = moment(time, 'H:m').format('HH:mm');

    const questionsConfigs = new Array<QuestionConfigurationDto>();

    this.pointsArray.controls.forEach((control, index) => {
      const questionConf = new QuestionConfigurationDto();
      questionConf.points = index + 1;
      questionConf.numberOfQuestions = control.value;
      questionsConfigs.push(questionConf);
    });

    config.questionsConfigurations = questionsConfigs;

    this.configService.create(config).then(() => {
      this.activeModal.close('added');
    });

  }

  editConfig() {
    if (!this.validate()) {
      return;
    }
    const config = new ConfigurationDto();
    config.percent = this.configForm.get('percent').value;
    config.totalWindowChangesAllow = this.configForm.get('windowChanges').value;

    let time = this.configForm.get('time').value;
    time = `${(time - time % 60) / 60}:${time % 60}`;
    config.time = moment(time, 'H:m').format('HH:mm');

    const questionsConfigs = new Array<QuestionConfigurationDto>();

    this.pointsArray.controls.forEach((control, index) => {
      if (this.pointsChecked[index]) {
        const questionConf = new QuestionConfigurationDto();
        questionConf.points = index + 1;
        questionConf.numberOfQuestions = control.value;
        questionsConfigs.push(questionConf);
      }
    });

    config.questionsConfigurations = questionsConfigs;

    this.configService.modify(config, this.config.id).then(() => {
      this.activeModal.close('edited');
    });
  }

}
