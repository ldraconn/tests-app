import { Component, OnInit } from '@angular/core';
import {Question} from '../../../models/question';
import {QuestionsService} from '../../../services/questions.service';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {QuestionModalContent} from '../questions-list/question-modal/question-modal.component';
import {ConfigurationService} from '../../../services/configuration.service';
import {Configuration} from '../../../models/configuration';
import {ConfigurationModal} from './configuration-modal/configuration-modal.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-configurations-list',
  templateUrl: './configurations-list.component.html',
  styleUrls: ['./configurations-list.component.scss']
})
export class ConfigurationsListComponent implements OnInit {

  isLoaded: boolean;
  activeConfig: Configuration
  configurations: Configuration[];
  pages: number;

  currentConfiguration: number;
  currentPage: number;

  constructor(public service: ConfigurationService,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
    this.configurations = [];
    this.currentConfiguration = -2;
    this.isLoaded = false;
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.service.getList(1, 10, 'id', 'asc').then(response => {
      this.configurations = response.content;
      this.activeConfig = this.configurations.filter(config => config.active)[0];
      this.configurations.splice(this.configurations.indexOf(this.activeConfig), 1);
      this.pages = response.totalPages;
      this.isLoaded = true;
    });
  }

  setNewPage(configurations: Configuration[]) {
    this.currentConfiguration = -2;
    this.configurations = configurations;

  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }

  openNewConfigModal(config?: Configuration) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    this.modalService.open(ConfigurationModal, ngbModalOptions).result.then(result => {
      this.refresh(result);
    });
  }

  refresh(result?) {
    this.service.getList(this.currentPage, 10, 'id', 'asc').then(response => {
      this.activeConfig = (response.content as Configuration[]).filter(config => config.active)[0];
      this.configurations = (response.content as Configuration[]).filter(config => !config.active);
      this.pages = response.totalPages;
      this.currentConfiguration = -2;
      this.showAlert(result);
    });
  }

  showAlert(result) {
    switch (result) {
      case 'added': {
        alert('Dodano konfigurację');
        break;
      }
      case 'edited': {
        alert('Zmodyfikowano konfigurację');
        break;
      }
      case 'toggled': {
        alert('Zmieniono aktualną konfigurację');
        break;
      }
      case 'deleted': {
        alert('Usunięto konfigurację');
        break;
      }
      default: {
        break;
      }
    }
  }

  toggle(index: number) {
    if (this.currentConfiguration === index) {
      this.currentConfiguration = -2;
    } else {
      this.currentConfiguration = index;
    }
  }

}
