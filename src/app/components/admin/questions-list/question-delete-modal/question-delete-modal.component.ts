import { QuestionsService } from '../../../../services/questions.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-question-delete-modal',
  templateUrl: './question-delete-modal.component.html',
  styleUrls: ['./question-delete-modal.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class QuestionDeleteModal implements OnInit {

  @Input() questionId: number;

  constructor(public activeModal: NgbActiveModal,
              private service: QuestionsService) { }

  ngOnInit() {
  }

  deleteQuestion() {
    this.service.delete(this.questionId).then(() => {
      this.activeModal.close('deleted');
    });
  }
}
