import { QuestionsService } from '../../../../services/questions.service';
import { QuestionDto } from '../../../../models/question-dto';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriesService } from '../../../../services/categories.service';
import { Category } from '../../../../models/category';
import { Question } from '../../../../models/question';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-question-modal',
  templateUrl: './question-modal.component.html',
  styleUrls: ['./question-modal.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class QuestionModalContent implements OnInit {

  question: Question;
  categories: Category[];

  questionForm = new FormBuilder().group({
    category: ['', Validators.required],
    points: ['', Validators.required],
    content: ['', Validators.required],
    correct: ['', Validators.required],
    answerA: ['', Validators.required],
    answerB: ['', Validators.required],
    answerC: ['', Validators.required],
    answerD: ['', Validators.required]
  });

  isEdited = {
    category: false,
    points: false,
    content: false,
    correct: false,
    answerA: false,
    answerB: false,
    answerC: false,
    answerD: false
  };

  constructor(public activeModal: NgbActiveModal,
              private categoriesService: CategoriesService,
              private questionsService: QuestionsService,
              private router: Router) {
    this.categories = [];
  }

  ngOnInit() {
    this.categoriesService.getCategoriesList().then(response => {
      this.categories = response;
      if (this.question) {
        this.questionForm = new FormBuilder().group({
          category: [this.question.categoryName, Validators.required],
          points: [this.question.points, Validators.required],
          content: [this.question.content, Validators.required],
          correct: [this.question.correct, Validators.required],
          answerA: [this.question.answerA, Validators.required],
          answerB: [this.question.answerB, Validators.required],
          answerC: [this.question.answerC, Validators.required],
          answerD: [this.question.answerD, Validators.required]
        });
      }
    });
  }

  validate() {
    this.isEdited = {
      category: true,
      points: true,
      content: true,
      correct: true,
      answerA: true,
      answerB: true,
      answerC: true,
      answerD: true
    };

    return !(this.questionForm.get('category').getError('required') ||
      this.questionForm.get('points').getError('required') ||
      this.questionForm.get('correct').getError('required') ||
      this.questionForm.get('content').getError('required') ||
      this.questionForm.get('answerA').getError('required') ||
      this.questionForm.get('answerB').getError('required') ||
      this.questionForm.get('answerC').getError('required') ||
      this.questionForm.get('answerD').getError('required'));
  }

  addQuestion() {
    if (!this.validate()) {
      return;
    }
    const question = new QuestionDto();
    question.content = this.questionForm.get('content').value;
    question.answerA = this.questionForm.get('answerA').value;
    question.answerB = this.questionForm.get('answerB').value;
    question.answerC = this.questionForm.get('answerC').value;
    question.answerD = this.questionForm.get('answerD').value;
    question.correct = this.questionForm.get('correct').value;
    question.points = this.questionForm.get('points').value;
    question.categoryId = this.categories.filter(category => category.name === this.questionForm.get('category').value)[0].id;
    this.questionsService.create(question).then(() => {
      this.activeModal.close('added');
    });
  }

  editQuestion() {
    if (!this.validate()) {
      return;
    }
    const question = new QuestionDto();
    question.content = this.questionForm.get('content').value;
    question.answerA = this.questionForm.get('answerA').value;
    question.answerB = this.questionForm.get('answerB').value;
    question.answerC = this.questionForm.get('answerC').value;
    question.answerD = this.questionForm.get('answerD').value;
    question.correct = this.questionForm.get('correct').value;
    question.points = this.questionForm.get('points').value;
    question.categoryId = this.categories.filter(category => category.name === this.questionForm.get('category').value)[0].id;
    this.questionsService.modify(question, this.question.id).then(() => {
      this.activeModal.close('edited');
    });
  }
}
