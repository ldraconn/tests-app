import { QuestionModalContent } from '../question-modal/question-modal.component';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Question } from '../../../../models/question';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { QuestionDeleteModal } from '../question-delete-modal/question-delete-modal.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() question: Question;
  @Output() closed = new EventEmitter<any>();

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  openEditQuestionModal(question: Question) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modelRef = this.modalService.open(QuestionModalContent, ngbModalOptions);
    modelRef.componentInstance.question = question;
    modelRef.result.then(result => {
      this.closed.emit(result);
    });
  }

  openDeleteQuestionModal() {

    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modalRef = this.modalService.open(QuestionDeleteModal, ngbModalOptions);
    modalRef.componentInstance.questionId = this.question.id;
    modalRef.result.then(result => {
      this.closed.emit(result);
    });
  }

}
