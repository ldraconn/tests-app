import { QuestionModalContent } from './question-modal/question-modal.component';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { QuestionsService } from './../../../services/questions.service';
import { Question } from './../../../models/question';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss']
})
export class QuestionsListComponent implements OnInit {

  isLoaded: boolean;
  questions: Question[];
  pages: number;

  currentQuestion: number;
  currentPage: number;

  constructor(public service: QuestionsService,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
    this.questions = [];
    this.currentQuestion = -1;
    this.isLoaded = false;
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.service.getList(this.currentPage, 10, 'id', 'asc').then(response => {
      this.questions = response.content;
      this.pages = response.totalPages;
      this.isLoaded = true;
    });
  }

  setNewPage(questionList: Question[]) {
    this.currentQuestion = -1;
    this.questions = questionList;

  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }

  openNewQuestionModal() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    this.modalService.open(QuestionModalContent, ngbModalOptions).result.then(result => {
      this.refresh(result);
    });
  }

  refresh(result) {
    this.service.getList(this.currentPage, 10, 'id', 'asc').then(response => {
      this.questions = response.content;
      this.pages = response.totalPages;
      this.showAlert(result);
    });
  }

  showAlert(result) {
    switch (result) {
      case 'added': {
        alert('Dodano pytanie');
        break;
      }
      case 'edited': {
        alert('Zmodyfikowano pytanie');
        break;
      }
      case 'deleted': {
        alert('Usunięto pytanie');
        break;
      }
    }
  }

  toggle(index: number) {
    if (this.currentQuestion === index) {
      this.currentQuestion = -1;
    } else {
      this.currentQuestion = index;
    }
  }

}
