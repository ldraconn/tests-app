import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiCrudService} from '../../../services/api-crud.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input() service: ApiCrudService;
  @Input() pages: number;
  @Input() sorting: boolean;
  @Input() currentPage: number;
  @Output() currentPageNumber = new EventEmitter<number>();
  @Output() newPage = new EventEmitter<any[]>();

  constructor(private router: Router,
              private activated: ActivatedRoute,
              private location: Location) {
    this.currentPage = 1;
  }

  ngOnInit() {
  }

  get getPagesList() {
    const list = [];
    for (let i = -2; i <= 2; i++) {
      if (this.currentPage + i >= 1 && this.currentPage + i <= this.pages) {
        list.push(this.currentPage + i);
      }
    }
    return list;
  }

  getPage(page: number) {
    this.service.getList(page, 10, this.sorting ? 'id' : null, this.sorting ? 'asc' : null).then(response => {
      this.currentPage = page;
      this.newPage.emit(response.content);
      this.currentPageNumber.emit(this.currentPage);
      let url = '/';
      this.activated.snapshot.url.forEach(segment => {
        if ( isNaN(parseInt(segment.path)) ) {
          url = `${url}${segment.path}/`;
        }
      });
      url = `${url}${this.currentPage}`;
      this.location.replaceState(
        this.router.createUrlTree(
          [url],
        ).toString()
      );
    });
  }

}
