import {Component, OnInit} from '@angular/core';
import {Category} from '../../../models/category';
import {CategoriesService} from '../../../services/categories.service';
import {CategoryForm} from '../../../models/forms/category-form';
import {CategoryDto} from '../../../models/category-dto';
import {Configuration} from '../../../models/configuration';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  isLoaded: boolean;
  categories: (Category | CategoryForm)[];

  editedCategories: Map<number, Category>;

  modifyFlags: boolean[];

  pages: number;
  currentPage: number;

  constructor(public service: CategoriesService,
              private route: ActivatedRoute) {
    this.categories = [];
    this.modifyFlags = [];
    this.editedCategories = new Map<number, Category>();
    this.isLoaded = false;
    this.pages = 0;
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.service.getList(1, 10, 'id', 'asc').then(response => {
      this.categories = response.content as Category[];
      this.modifyFlags = Array<boolean>(this.categories.length).fill(false);
      this.isLoaded = true;
      this.pages = response.totalPages;
    });
  }

  setNewPage(categories: Category[]) {
    this.categories = categories;
    this.editedCategories.clear();
    this.modifyFlags = Array<boolean>(this.categories.length).fill(false);
  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }

  addForm() {
    this.categories.push(new CategoryForm());
    this.modifyFlags.push(true);
  }

  applyCategory(index: number) {

    const edited = this.categories[index] as CategoryForm;

    this.service.getOne(Number(edited.id)).then(() => {
      this.service.modify(new CategoryDto(edited.name.value), Number(edited.id)).then(modified => {
        this.categories[index] = modified as Category;
        this.modifyFlags[index] = false;
      });
    }).catch(error => {
      if (error.status === 404) {
        this.service.create(new CategoryDto(edited.name.value)).then(created => {
          this.categories[index] = created as Category;
          this.modifyFlags[index] = false;
        });
      }
    });

  }

  editCategory(index: number) {
    this.editedCategories.set(index, this.categories[index] as Category);
    this.categories[index] = new CategoryForm(this.categories[index] as Category);
    this.modifyFlags[index] = true;
  }

  cancel(index: number) {
    if (this.editedCategories.get(index)) {
      this.categories[index] = this.editedCategories.get(index) as Category;
      this.editedCategories.delete(index);
      this.modifyFlags[index] = false;
    } else {
      this.categories.splice(index, 1);
      this.modifyFlags.splice(index, 1);
    }
  }

  toggleCategory(index: number) {
    const selected = this.categories[index] as Category;
    const body = new CategoryDto(selected.name, !selected.active);

    this.service.modify(body, selected.id).then(response => {
      this.categories[index] = response;
    });
  }
}
