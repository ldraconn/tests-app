import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {QuestionsService} from '../../../../services/questions.service';
import {UsersService} from '../../../../services/users.service';

@Component({
  selector: 'app-user-delete-modal',
  templateUrl: './user-delete-modal.component.html',
  styleUrls: ['./user-delete-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class UserDeleteModal implements OnInit {

  questionId: number;

  constructor(public activeModal: NgbActiveModal,
              private service: UsersService) { }

  ngOnInit() {
  }

  deleteUser() {
    this.service.delete(this.questionId).then(() => {
      this.activeModal.close('deleted');
    });
  }

}
