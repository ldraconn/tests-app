import {Component, OnInit} from '@angular/core';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../models/user';
import {UsersService} from '../../../services/users.service';
import {UserModalContent} from './user-modal/user-modal.component';
import {UserDeleteModal} from './user-delete-modal/user-delete-modal.component';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  private jwtService = new JwtHelperService();

  isLoaded: boolean;
  users: User[];
  pages: number;

  currentUser: number;
  currentPage: number;
  currentOrder: string;
  currentDir: string;

  constructor(public service: UsersService,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
    this.users = [];
    this.currentUser = -1;
    this.isLoaded = false;
    this.currentOrder = '';
    this.currentDir = '';
    route.params.subscribe(params => {
      params['id'] ? this.currentPage = +params['id'] : this.currentPage = 1;
    });
  }

  ngOnInit() {
    this.service.getList(1, 10, 'id', 'asc').then(response => {
      this.users = response.content;
      this.pages = response.totalPages;
      this.isLoaded = true;
      this.currentOrder = 'id';
      this.currentDir = 'asc';
    });
  }

  setNewPage(usersList: User[]) {
    this.currentUser = -1;
    this.users = usersList;

  }

  setCurrentPage(current: number) {
    this.currentPage = current;
  }

  sortBy(values) {
    this.currentOrder = values.order;
    this.currentDir = values.dir;
    this.service.getList(this.currentPage, 10, this.currentOrder, this.currentDir).then(response => {
      this.users = response.content;
    });
  }

  openNewUserModal(user?: User) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modelRef = this.modalService.open(UserModalContent, ngbModalOptions);
    if (user) {
      modelRef.componentInstance.user = user;
    }
    modelRef.componentInstance.adminRole = this.jwtService.decodeToken(localStorage.getItem('userToken')).authorities[0];
    modelRef.result.then(result => {
      this.refresh(result);
    });
  }

  openDeleteUserModal(index: number) {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modalRef = this.modalService.open(UserDeleteModal, ngbModalOptions);
    modalRef.componentInstance.questionId = this.users[index].id;
    modalRef.result.then(result => {
      this.refresh('deleted');
    });
  }

  refresh(result) {
    this.service.getList(this.currentPage, 10, 'id', 'asc').then(response => {
      this.users = response.content;
      this.pages = response.totalPages;
      this.showAlert(result);
    });
  }

  showAlert(result) {
    switch (result) {
      case 'added': {
        alert('Dodano użytkownika');
        break;
      }
      case 'edited': {
        alert('Zmodyfikowano użytkownika');
        break;
      }
      case 'deleted': {
        alert('Usunięto użytkownika');
        break;
      }
    }
  }
}
