import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../../models/user';
import {UsersService} from '../../../../services/users.service';
import {UserDto} from '../../../../models/user-dto';
import {AdminRole} from '../../../../models/enums/admin-role.enum';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class UserModalContent implements OnInit {

  user: User;
  adminRole: AdminRole;
  passChecked: boolean;

  userForm = new FormBuilder().group({
    email: ['', Validators.required, Validators.email],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    password: ['', Validators.required],
    passwordRepeat: ['', Validators.required],
    role: ['ROLE_USER', Validators.required],
  });

  isEdited = {
    email: false,
    firstName: false,
    lastName: false,
    password: false,
    passwordRepeat: false,
    role: false
  };

  constructor(public activeModal: NgbActiveModal,
              private userService: UsersService) {
    this.passChecked = false;
  }

  ngOnInit() {
    if (this.user) {
      this.userForm = new FormBuilder().group({
        email: [this.user.email, Validators.required],
        firstName: [this.user.firstName, Validators.required],
        lastName: [this.user.lastName, Validators.required],
        password: ['', Validators.required],
        passwordRepeat: ['', Validators.required],
        role: [this.user.role, Validators.required],
      });

      switch (this.adminRole) {
        case AdminRole.ROLE_ADMIN: {
          this.userForm.get('role').disable();
          break;
        }
        case AdminRole.ROLE_SUPER_ADMIN: {

          break;
        }
      }

      if (this.userForm.get('role').value === AdminRole.ROLE_SUPER_ADMIN) {
        this.userForm.get('role').disable();
      }
    }
  }

  validate() {
    this.isEdited = {
      email: true,
      firstName: true,
      lastName: true,
      password: true,
      passwordRepeat: true,
      role: true
    };

    return !(this.userForm.get('email').getError('required') ||
      this.userForm.get('firstName').getError('required') ||
      this.userForm.get('lastName').getError('required') ||
      (this.userForm.get('password').getError('required') && this.passChecked ) ||
      (this.userForm.get('passwordRepeat').getError('required') && this.passChecked ) ||
      this.userForm.get('role').getError('required') ||
      this.userForm.get('password').value !== this.userForm.get('passwordRepeat').value);
  }

  addUser() {
    if (!this.validate()) {
      return;
    }
    const user = new UserDto();
    user.email = this.userForm.get('email').value;
    user.firstName = this.userForm.get('firstName').value;
    user.lastName = this.userForm.get('lastName').value;
    user.password = this.userForm.get('password').value;
    this.userService.create(user).then(response => {
      this.activeModal.close('added');
    });

  }

  editUser() {
    if (!this.validate()) {
      return;
    }
    const user = new UserDto();
    user.email = this.userForm.get('email').value;
    user.firstName = this.userForm.get('firstName').value;
    user.lastName = this.userForm.get('lastName').value;
    user.password = this.userForm.get('password').value;
    user.role = this.userForm.get('role').value;
    this.userService.modify(user, this.user.id).then(() => {
      this.activeModal.close('edited');
    });
  }

}
