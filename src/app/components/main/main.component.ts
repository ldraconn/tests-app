import { TestService } from './../../services/test.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  hasTest: number;

  constructor(private service: TestService,
              private router: Router) {
    this.hasTest = 0;
  }

  ngOnInit() {
    this.service.getCurrentTest().then(() => this.hasTest++).catch(() => this.hasTest--);
  }

  getTest() {
    if (this.hasTest < 0) {
      this.service.startTest().then(() => {
        this.router.navigate(['/test']);
      });
    } else if (this.hasTest > 0) {
      this.router.navigate(['/test']);
    }
  }
}
