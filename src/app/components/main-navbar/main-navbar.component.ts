import {Component, OnInit} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss']
})
export class MainNavbarComponent implements OnInit {

  environment = environment;

  private jwtService = new JwtHelperService();

  constructor(private router: Router) {
  }

  ngOnInit() {
    environment.isAdmin = (this.jwtService.decodeToken(localStorage.getItem('userToken')).authorities as any[])
      .filter(role => role === 'ROLE_ADMIN' || role === 'ROLE_SUPER_ADMIN').length !== 0;
  }

  logout() {
    localStorage.removeItem('userToken');
    environment.isLogged = false;
    environment.isAdmin = false;
    this.router.navigate(['/login']);
  }

}
