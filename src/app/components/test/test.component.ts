import {TestService} from '../../services/test.service';
import {Test} from '../../models/test';
import {Component, HostListener, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ReportsService} from '../../services/reports.service';
import {ReportDto} from '../../models/report-dto';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ReportModal} from './report-modal/report-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  isLoaded: boolean;

  currentTest: Test;
  currentQuestion: number;
  timeMinutes: number;
  timeSeconds: number;
  isFinished: boolean;

  interval: any;

  constructor(private testService: TestService,
              private modalService: NgbModal,
              private router: Router) {
    this.currentQuestion = 0;
    this.isFinished = false;
    this.isLoaded = false;
  }

  ngOnInit() {
    this.testService.getCurrentTest().then(response => {
      this.currentTest = response;
      const start = moment(this.currentTest.now);
      const end = moment(this.currentTest.expirationAt);
      let timeEnd = (end.valueOf() - end.valueOf() % 1000) / 1000;
      timeEnd = timeEnd - (start.valueOf() - start.valueOf() % 1000) / 1000;
      this.isLoaded = true;
      this.interval = setInterval(() => {
        let time = timeEnd--;
        this.timeSeconds = time % 60;
        time = time - this.timeSeconds;
        this.timeMinutes = time / 60;
        if (timeEnd < 0) {
          clearInterval(this.interval);
          this.testService.finishTest().then(finished => {
            this.currentTest = finished;
            this.isFinished = true;
          });
        }
      }, 1000);
    }).catch(error => {
      this.router.navigate(['/']);
    });
  }

  prev() {
    if (this.currentQuestion !== 0) {
      this.currentQuestion--;
    }
  }

  selectAnswer(answer: string) {
    const id = this.currentTest.answers[this.currentQuestion].id;
    this.testService.chooseAnswer(id, answer).then(ans => {
      this.currentTest.answers[this.currentQuestion] = ans;
    });
  }

  finishTest() {
    this.testService.finishTest().then(response => {
      this.currentTest = response;
      this.isFinished = true;
    });
  }

  next() {
    if (this.currentQuestion !== this.currentTest.answers.length - 1) {
      this.currentQuestion++;
    }
  }

  showReportForm() {
    const ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      windowClass: 'myCustomModalClass'
    };
    const modalRef = this.modalService.open(ReportModal, ngbModalOptions);
    modalRef.componentInstance.question = this.currentTest.answers[this.currentQuestion].question;
    modalRef.result.then(() => {
      alert('Zgłoszenie wysłane');
    });
  }

  @HostListener('window:blur')
  detectWindowChange() {
    if (!this.isFinished) {
      this.testService.detectCheating();
    }
  }
}
