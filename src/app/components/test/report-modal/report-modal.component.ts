import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ReportsService} from '../../../services/reports.service';
import {FormControl, Validators} from '@angular/forms';
import {ReportDto} from '../../../models/report-dto';
import {Question} from '../../../models/question';
import {QuestionResponse} from '../../../models/question-response';

@Component({
  selector: 'app-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ReportModal implements OnInit {

  content: FormControl;
  isContentEdited: boolean;

  question: Question | QuestionResponse;

  constructor(public activeModal: NgbActiveModal,
              private service: ReportsService) {
    this.content = new FormControl('', [Validators.required]);
    this.isContentEdited = false;
  }

  ngOnInit() {
  }

  report() {
    const report = new ReportDto(this.content.value, this.question.id);

    this.isContentEdited = true;

    if (!this.content.getError('required')) {
      this.service.create(report).then(() => {
        this.activeModal.close();
      });
    }

  }

}
