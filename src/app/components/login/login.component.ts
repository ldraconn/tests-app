import {AuthService} from './../../services/auth.service';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private jwtService = new JwtHelperService();

  invalid = false;
  loading = false;

  loginForm = this.builder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(
    private builder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    const loginData = this.loginForm.value;

    this.loading = true;
    this.authService.login(loginData.username, loginData.password).then(response => {
      localStorage.setItem('userToken', response.access_token);
      if ((this.jwtService.decodeToken(response.access_token).authorities as any[])
        .filter(role => role === 'ROLE_ADMIN' || role === 'ROLE_SUPER_ADMIN').length !== 0) {
        environment.isAdmin = true;
        this.router.navigate(['admin/questions']);
      } else {
        environment.isAdmin = false;
        this.router.navigate(['']);
      }
        this.loading = false;
    }).catch(error => {
      this.invalid = true;
      this.loading = false;
    });
  }

}
