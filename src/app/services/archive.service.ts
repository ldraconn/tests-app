import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Test} from '../models/test';
import {ApiCrudService} from './api-crud.service';

@Injectable({
  providedIn: 'root'
})
export class ArchiveService extends ApiCrudService{

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/test';
    this.routePlural = 'api/tests';
  }

  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  getOne(id: number): Promise<Test> {
    return super.getOne(id).then(response => response as Test);
  }
}
