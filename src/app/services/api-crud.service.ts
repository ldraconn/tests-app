import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class ApiCrudService {

  protected route: string;
  protected routePlural: string;

  protected constructor(protected http: HttpClient) {
  }

  getList(page?: number, size?: number, order?: string, dir?: string) {
    let url = `${environment.apiUrl}/${this.routePlural}`;
    if (page) {
      url = `${url}/${page}`;
    }
    if (size) {
      url = `${url}/${size}`;
    }
    if (order) {
      url = `${url}/${order}`;
    }
    if (dir) {
      url = `${url}/${dir}`;
    }

    return this.http.get(url).toPromise().then(response => response as any);
  }

  getOne(id: number) {
    return this.http.get(`${environment.apiUrl}/${this.route}/${id}`).toPromise().then(response => response as any);
  }

  create(body: any) {
    return this.http.post(`${environment.apiUrl}/${this.route}`, body).toPromise().then(response => response as any);
  }

  modify(body: any, id: number) {
    return this.http.patch(`${environment.apiUrl}/${this.route}/${id}`, body).toPromise().then(response => response as any);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/${this.route}/${id}`).toPromise().then(response => response as any);
  }

}
