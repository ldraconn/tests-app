import { Injectable } from '@angular/core';
import {ApiCrudService} from './api-crud.service';
import {HttpClient} from '@angular/common/http';
import {Configuration} from '../models/configuration';
import {ConfigurationDto} from '../models/configuration-dto';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService extends ApiCrudService{

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/config';
    this.routePlural = 'api/configs';
  }

  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  getOne(id: number): Promise<Configuration> {
    return super.getOne(id).then(response => response as Configuration);
  }

  create(body: ConfigurationDto): Promise<Configuration> {
    return super.create(body).then(response => response as Configuration);
  }

  modify(body: ConfigurationDto, id: number): Promise<Configuration> {
    return super.modify(body, id).then(response => response as Configuration);
  }

  delete(id: number): Promise<Configuration> {
    return super.delete(id).then(response => response as Configuration);
  }
}
