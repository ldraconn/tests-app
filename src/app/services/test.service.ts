import { Test } from './../models/test';
import { Answer } from './../models/answer';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  chooseAnswer(id: number, answer: string) {
    return this.http.patch(`${environment.apiUrl}/api/answer/choose/${id}/${answer}`, {})
      .toPromise().then(response => response as Answer);
  }

  detectCheating() {
    return this.http.patch(`${environment.apiUrl}/api/test/cheating`, {})
      .toPromise().then(response => response as Test);
  }

  getCurrentTest() {
    return this.http.get(`${environment.apiUrl}/api/test/current`)
      .toPromise().then(response => response as Test);
  }

  startTest() {
    return this.http.post(`${environment.apiUrl}/api/test/start`, {})
      .toPromise().then(response => response as Test);
  }

  finishTest() {
    return this.http.post(`${environment.apiUrl}/api/test/finish`, {})
      .toPromise().then(response => response as Test);
  }
}
