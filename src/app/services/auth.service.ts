import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HeadersBuilder } from 'src/app/utils/headers-builder';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private clientId = '092d1d6ea3a42236f66e7ac30c9631f7a68513c7b48aae3025b900be238d1954';
  private clientSecret = '3e8bece9c508ac904047ec10ed0893d10a07fd19a4889ba6e381cc74d1ecb0d2';

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    const headers = new HeadersBuilder()
      .contentType('application/json')
      .authorization('Basic ' + btoa(`${this.clientId}:${this.clientSecret}`))
      .build();
    return this.http.post(`${environment.apiUrl}/oauth/token`,
      { username: username, password: password, grant_type: 'password' },
      { headers: headers }).toPromise().then(response => response as any);
  }
}
