import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ReportDto} from '../models/report-dto';
import {Report} from '../models/report';
import {ApiCrudService} from './api-crud.service';

@Injectable({
  providedIn: 'root'
})
export class ReportsService extends ApiCrudService{

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/report';
    this.routePlural = 'api/reports';
  }


  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  create(body: ReportDto): Promise<Report> {
    return super.create(body).then(response => response as Report);
  }

  moderateReport(id: number, positive: boolean) {
    return this.http.patch(`${environment.apiUrl}/${this.route}/${id}/${positive}`, {})
      .toPromise().then(response => response as Report);
  }
}
