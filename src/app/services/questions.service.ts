import { QuestionDto } from './../models/question-dto';
import { Question } from './../models/question';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ApiCrudService} from './api-crud.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService extends ApiCrudService{

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/question';
    this.routePlural = 'api/questions';
  }


  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  getOne(id: number): Promise<Question> {
    return super.getOne(id).then(response => response as Question);
  }

  create(body: QuestionDto): Promise<Question> {
    return super.create(body).then(response => response as Question);
  }

  modify(body: QuestionDto, id: number): Promise<Question> {
    return super.modify(body, id).then(response => response as Question);
  }

  delete(id: number): Promise<Question> {
    return super.delete(id).then(response => response as Question);
  }
}
