import { Category } from './../models/category';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CategoryDto } from '../models/category-dto';
import { ApiCrudService } from './api-crud.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends ApiCrudService{

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/category';
    this.routePlural = 'api/categories';
  }


  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  getCategoriesList(): Promise<Category[]> {
    return this.http.get(`${environment.apiUrl}/api/categories/list`).toPromise()
        .then(response => response as Category[]);
  }

  getOne(id: number): Promise<Category> {
    return super.getOne(id).then(response => response as Category);
  }

  create(body: CategoryDto): Promise<Category> {
    return super.create(body).then(response => response as Category);
  }

  modify(body: CategoryDto, id: number): Promise<Category> {
    return super.modify(body, id).then(response => response as Category);
  }
}
