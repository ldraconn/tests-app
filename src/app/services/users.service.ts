import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HeadersBuilder} from '../utils/headers-builder';
import {environment} from '../../environments/environment';
import {User} from '../models/user';
import {UserDto} from '../models/user-dto';
import {ApiCrudService} from './api-crud.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends ApiCrudService {

  constructor(protected http: HttpClient) {
    super(http);
    this.route = 'api/user';
    this.routePlural = 'api/users';
  }

  getList(page?: number, size?: number, order?: string, dir?: string): Promise<any> {
    return super.getList(page, size, order, dir).then(response => response as any);
  }

  getOne(id: number): Promise<User> {
    return super.getOne(id).then(response => response as User);
  }

  create(body: UserDto): Promise<User> {
    return super.create(body).then(response => response as User);
  }

  modify(body: UserDto, id: number): Promise<User> {
    return super.modify(body, id).then(response => response as User);
  }

  delete(id: number): Promise<User> {
    return super.delete(id).then(response => response as User);
  }
}
