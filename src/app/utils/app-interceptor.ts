import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {HeadersBuilder} from './headers-builder';

@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const headers = new HeadersBuilder()
      .contentType('application/json')
      .authorization(`Bearer ${localStorage.getItem('userToken')}`)
      .build();

    let authReq;

    if (req.url !== `${environment.apiUrl}/oauth/token`) {
      authReq = req.clone({headers: headers});
    } else {
      authReq = req.clone();
    }

    return next.handle(authReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          environment.isLogged = false;
          this.router.navigate(['/login']);
          return throwError(error);
        }
        return throwError(error);
      })
    );


  }

}
