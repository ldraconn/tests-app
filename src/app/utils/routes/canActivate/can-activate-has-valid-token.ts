import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';

@Injectable()
export class CanActivateHasValidToken implements CanActivate {

  constructor(private router: Router) {}

  private jwtService = new JwtHelperService();

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = localStorage.getItem('userToken');

    const isToken = token && !this.jwtService.isTokenExpired(token);

    if (!isToken) {
      environment.isLogged = false;
      this.router.navigate(['/login']);
    } else {
      environment.isLogged = true;
    }
    return isToken;
  }

}
