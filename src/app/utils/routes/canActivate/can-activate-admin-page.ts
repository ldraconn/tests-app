import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';

@Injectable()
export class CanActivateAdminPage implements CanActivate {

  private jwtService = new JwtHelperService();

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const isAdmin = (this.jwtService.decodeToken(localStorage.getItem('userToken')).authorities as any[])
      .filter(role => role === 'ROLE_ADMIN' || role === 'ROLE_SUPER_ADMIN').length !== 0;

    if (!isAdmin) {
      this.router.navigate(['/']);
    }

    return isAdmin;
  }
}
