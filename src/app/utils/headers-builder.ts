import { HttpHeaders } from '@angular/common/http';
export class HeadersBuilder {
  headers: HttpHeaders;

  constructor() {
    this.headers = new HttpHeaders();
  }

  contentType(content: string): HeadersBuilder {
    this.headers = this.headers.append('Content-Type', content);
    return this;
  }

  authorization(content: string) {
    this.headers = this.headers.append('Authorization', content);
    return this;
  }

  build(): HttpHeaders {
    return this.headers;
  }
}
