import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'configTime'
})
export class ConfigTimePipe implements PipeTransform {

  transform(value: string): number {
    const mom = moment(value, 'HH:mm');

    return mom.hours() * 60 + mom.minutes();
  }

}
