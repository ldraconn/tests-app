import { CategoriesListComponent } from './components/admin/categories-list/categories-list.component';
import { TestComponent } from './components/test/test.component';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionsListComponent } from './components/admin/questions-list/questions-list.component';
import { CanActivateAdminPage} from './utils/routes/canActivate/can-activate-admin-page';
import {CanActivateHasValidToken} from './utils/routes/canActivate/can-activate-has-valid-token';
import {UsersListComponent} from './components/admin/users-list/users-list.component';
import {ArchiveListComponent} from './components/admin/archive-list/archive-list.component';
import {ReportsListComponent} from './components/admin/reports-list/reports-list.component';
import {ConfigurationsListComponent} from './components/admin/configurations-list/configurations-list.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: MainComponent, canActivate: [CanActivateHasValidToken] },
  { path: 'test', component: TestComponent, canActivate: [CanActivateHasValidToken]},
  { path: 'admin/questions', component: QuestionsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/questions/:id', component: QuestionsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/categories', component: CategoriesListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/categories/:id', component: CategoriesListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/users', component: UsersListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/users/:id', component: UsersListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/archive', component: ArchiveListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/archive/:id', component: ArchiveListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'my-tests', component: ArchiveListComponent, canActivate: [CanActivateHasValidToken] },
  { path: 'my-tests/:id', component: ArchiveListComponent, canActivate: [CanActivateHasValidToken] },
  { path: 'admin/reports', component: ReportsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/reports/:id', component: ReportsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/configs', component: ConfigurationsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] },
  { path: 'admin/configs/:id', component: ConfigurationsListComponent, canActivate: [CanActivateAdminPage, CanActivateHasValidToken] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
