export const environment = {
  apiUrl: 'http://localhost:8888/quiz',
  isLogged: false,
  isAdmin: false,
  production: true
};
